import createServer from "./lib/app.js";
import 'tsconfig-paths/register.js';

const start = async () => {
	try {
		await createServer();
		console.log(`\n🚀 SERVER-LISTEN: localhost:${process.env.PORT}/api/v1`);
        console.log(`🔒 NODE-ENV: ${process.env.NODE_ENV}`, `| STATUS: ACTIVE 🟢\n`)
	} catch (error) {
		console.log(error);
	}
};

start();
