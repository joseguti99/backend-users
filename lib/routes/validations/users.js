import { Joi } from 'express-validation';

export default {
    getUserById: {
        params: Joi.object({
            id: Joi.number().required(),
        }),
    },
    getUserBySearch: {
        params: Joi.object({
            search: Joi.string().required(),
        }),
    }
}