import express from 'express'
import makeExpressCallback from '../helpers/makeExpressCallBack.js';
import validations from './validations/users.js'
import controller from '../controllers/users.js'
import validate from '../helpers/validateRequest.js'

const router = express.Router();

router
    .route(`/`)
    .get(makeExpressCallback(controller.getUsers));
    // example = localhost:3001/api/v1/users
/**
 * @swagger
 * tags:
 *   name: Users
 *   description: API to manage users.
 */

/**
 * @swagger
 * /users:
 *   get:
 *     summary: Retrieve a list of users
 *     tags: [Users]
 *     responses:
 *       200:
 *         description: A list of users.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: The user ID.
 *                     example: 1
 *                   name:
 *                     type: string
 *                     description: The user's name.
 *                     example: John Doe
 */

router
    .route(`/:id`)
    .get(validate(validations.getUserById), makeExpressCallback(controller.getUserById));
    // example = localhost:3001/api/v1/users/1

    /**
 * @swagger
 * /users/{id}:
 *   get:
 *     summary: Retrieve a single user by ID
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: id
 *         schema:
 *           type: integer
 *         required: true
 *         description: The user ID.
 *     responses:
 *       200:
 *         description: A single user.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 id:
 *                   type: integer
 *                   description: The user ID.
 *                   example: 1
 *                 name:
 *                   type: string
 *                   description: The user's name.
 *                   example: John Doe
 *       404:
 *         description: User not found.
 */

router
    .route(`/search/:search`)
    .get(validate(validations.getUserBySearch), makeExpressCallback(controller.getBySearch));

    // example = localhost:3001/api/v1/users/search/John
/**
 * @swagger
 * /users/search/{search}:
 *   get:
 *     summary: Search for users by name
 *     tags: [Users]
 *     parameters:
 *       - in: path
 *         name: search
 *         schema:
 *           type: string
 *         required: true
 *         description: The name to search for.
 *     responses:
 *       200:
 *         description: A list of users matching the search criteria.
 *         content:
 *           application/json:
 *             schema:
 *               type: array
 *               items:
 *                 type: object
 *                 properties:
 *                   id:
 *                     type: integer
 *                     description: The user ID.
 *                     example: 1
 *                   name:
 *                     type: string
 *                     description: The user's name.
 *                     example: John
 *       404:
 *         description: No users found.
 */

export default router;