import fs from "fs";
import path from "path";
import express from "express";
import { fileURLToPath } from "url";

const router = express.Router();

const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

router.use((req, res, next) => {
  if (req.method === "OPTIONS") {
    return next();
  }
  req.originalUrl = req.originalUrl.replace("//", "/");
  return next();
});

fs.readdirSync(__dirname)
  .filter((file) => file.indexOf(`.`) !== 0 && file !== `index.js` && file !== `validations`)
  .forEach((file) => {
    const modulePath = path.join(__dirname, file);
    const moduleUrl = `file://${modulePath}`;
    router.use(
      `/${file.replace(".js", "")}`,
      async (req, res, next) => {
        try {
          const importedModule = await import(moduleUrl);
          importedModule.default(req, res, next);
        } catch (error) {
          next(error);
        }
      }
    );
});

export default router;
