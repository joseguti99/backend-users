import UserService from '../services/users.js';
import injectionChecker  from '../helpers/injectionChecker.js'

async function getUsers(req, res, next) {
    const useCase = new UserService()
    const results = await useCase.getMany();
    return {
        statusCode: 200,
        body: results
    }
}

async function getUserById(req, res, next) {
    const { id } = req.params
    await injectionChecker(id)
    const useCase = new UserService()
    const results = await useCase.getUserById(id);
    return {
        statusCode: 200,
        body: results
    }
}

async function getBySearch (req, res, next){
    const { search } = req.params
    await injectionChecker(search)
    const useCase = new UserService()
    const results = await useCase.searchUser(search);
    return {
        statusCode: 200,
        body: results
    }
}


export default {
    getUsers,
    getUserById,
    getBySearch,
}
