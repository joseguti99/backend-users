import { AttributeValidationError } from "./errors.js";

export default function validate(schema){
	return (req, res, next) => {
		console.log('\x1b[32m', `${req.method} - url:${req.originalUrl}`, '\x1b[0m');

		const { error } = schema.params.validate(req.params);

		if(error) {
			const validationError = new AttributeValidationError(error.details)
			console.log(validationError?.error[0])
			return res.status(validationError.status).json({ error: validationError?.error[0] });
		}
		
		next()
	}
};