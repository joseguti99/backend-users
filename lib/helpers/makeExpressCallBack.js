import { ServerError } from "./errors.js"

export default function makeExpressCallback(controller) {
    return async (req, res) => {
        try {
            const httpRequest = {
                body: req.body,
                files: req.files,
                query: req.query,
                params: req.params,
                method: req.method,
                route: req.baseUrl + req.route.path,
                user: req.user,
                headers: {
                    Accept: req.get('x-accept'),
                    Authorization: req.get('Authorization'),
                    site_id: req.get('x-site-id') || undefined,
                },
            };
            const httpResponse = await controller(httpRequest);

            if (httpResponse.headers) {
                res.set(httpResponse.headers);
            }
			
            if (httpResponse.type) {
                res.type(httpResponse.type);
                res.status(httpResponse.statusCode).send(httpResponse.files).end();
            } else {
                res.type('json');
                res.status(httpResponse.statusCode).send(httpResponse.body).end();
            }
        } catch (error) {
            let customError = error;
            console.log('\x1b[31m', error, '\x1b[0m');
            if (!error.status) {
                customError = new ServerError(error);
            }
            const {
                status = 500,
                name,
                path,
                type,
                message = 'Internal server error',
            } = customError;
            res
                .status(status)
                .send({
                    ...(name ? { name } : {}),
                    ...(path ? { path } : {}),
                    ...(type ? { type } : {}),
                    ...(message ? { message } : {}),
                })
                .end();
        }
    };
};