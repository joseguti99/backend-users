import { AttributeValidationError } from "./errors.js";

export default async function injectionChecker(query) {
	// eslint-disable-next-line no-useless-escape
	const forbiddenChars = /[\^\*\'\;\,\{}\[\]\\(\)\|\n\r]/gm;

	Object.values(query).forEach((value) => {
		if (forbiddenChars.test(value)) {
			throw new AttributeValidationError(
				'Algún parámetro incluye caracteres no válidos'
			);
		}
	});
}