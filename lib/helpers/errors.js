export class ServerError extends Error {
	constructor(error) {
		if (error.name === `QueryFailedError`) return `new DatabaseError(error)`;
		super(`Error interno del servidor`);
		this.name = `Error: Servidor`;
		this.status = 500;
	}
}

export class RequiredParamError extends Error {
	constructor(param) {
		super(`The param ${param} is required`);
		this.name = `Error: Parámetro requerido`;
		this.status = 400;
	}
}

export class AttributeValidationError extends Error {
	constructor(details) {
		super(`La información enviada es inválida.`);
		this.name = `Error: Atributo inválido. ${details}`;
		this.status = 400;
		this.error = details;
	}
}

export class DataBaseError extends Error {
	constructor(details) {
		super(`Error en la consulta a la base de datos.`);
		this.name = `Error: Query inválida`;
		this.status = 400;
		this.error = details;
	}
}
