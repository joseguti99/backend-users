import swaggerJSDoc from "swagger-jsdoc";
import config from "../config/index.js";

export const swaggerOptions = swaggerJSDoc({
    definition: {
        openapi: '3.0.0',
        info: {
            title: 'api-text-to-sql',
            version: '1.0.0',
            description: 'Api de usuarios',
        },
        servers: [
            {
                url: `http://localhost:3001${config.PROJECT_URI}`,
                description: 'Local Server 🚀',
            }
        ],
        components: {
            securitySchemes: {
                bearerAuth: {
                    type: 'http',
                    scheme: 'bearer',
                    bearerFormat: 'JWT',
                },
            },
        },
    },
    apis: ['./lib/routes/*.js']
});