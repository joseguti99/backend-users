import axios from 'axios'
import config from '../config/index.js'

const axiosInstance = axios.create({
    baseURL: config.URL_API_USERS,
    maxBodyLength: Infinity,
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        // Authorization: `Bearer YOUR BEARER`
    }
}) 

export default axiosInstance