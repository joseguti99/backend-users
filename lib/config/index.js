export default {
    PROJECT_URI: process.env.PROJECT_URI || '/',
    URL_API_USERS: process.env.URL_API_USERS || '',
    BEARER_TOKEN: process.env.BEARER_TOKEN || '',
}