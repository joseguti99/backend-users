import express from 'express'
import cookieParser from 'cookie-parser';
import bodyParser from 'body-parser';
import cors from 'cors'
import config from './config/index.js'
import multer from 'multer';
import routes from './routes/index.js'
import { createNamespace } from 'cls-hooked';
import swaggerUi from 'swagger-ui-express'
import { swaggerOptions } from './helpers/swagger.js';

export default async function createServer() {
	const app = express();
	const upload = multer({ limits: { fileSize: 8000000 } });
	const namespace = createNamespace(`session`);

	// session handlers
	const handleContext = (req, res, next) => namespace.run(() => next());

	//middleware
	app.use(bodyParser.json({ limit: `50mb`, extended: true }));
	app.use(bodyParser.urlencoded({ limit: `50mb`, extended: true }));
	app.use(bodyParser.json());
	app.use(upload.any());
	app.use(cors());
	app.use(cookieParser());
	app.use(handleContext);
	app.use(express.json());
	app.use(`${config.PROJECT_URI}`, routes);

	app.use(`${config.PROJECT_URI}docs`, swaggerUi.serve, swaggerUi.setup(swaggerOptions));
	
	app.get(`${config.PROJECT_URI}api-docs/swagger.json`, (req, res) => res.json(swaggerOptions));

	if (!config.PORT) config.PORT = 3001;

	return app.listen(config.PORT);
}
