import config from "../config/index.js";
import axiosInstance from "../helpers/axiosInstance.js";
import { ServerError } from "../helpers/errors.js";

const api_url = `${config.URL_API_USERS}`;

export default class UserService {
    async getMany() {
        try {
            const { data, status } = await axiosInstance.get(`${api_url}/users`)
            if (status !== 200) return new ServerError(data?.response)
            return { status: 200, body: data }
        } catch (error) {
            new ServerError(error)
        }
    }

    async getUserById(id) {
        const { status, data } = await axiosInstance.get(`${api_url}/users/${id}`)
        if (status !== 200) return new ServerError(data?.response)
        return { status: 200, body: data }
    }
    
    async searchUser(search){
        const { data, status } = await axiosInstance.get(`${api_url}/users/search?q=${search}`)
        if (status !== 200) return new ServerError(data?.response)
        return { status: 200, body: data }
    }
}

